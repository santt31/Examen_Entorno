Hola! Soy Santiago Bruno.
=========================

Los pasos a seguir son los siguientes:
--------------------------------------

$git clone  examen && cd examen
$vagrant up 																#Levantará las dos maquina 
$vagrant ssh centos
$ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts_adm  #Provisiono VM adm
$ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts_aws	 #Provisiono a mi aws
$exit

VERIFICACIONES:
---------------


														### VM ADM
														-----------
$vagrant ssh admin
-verificamos que esta instalado el apache-
$service httpd start
-verificamos que se han instalado los modulos - php-xml,php-soap,php-bcmath
-----
$cat /etc/php.ini
(se podran ver los valores como ○ short_open_tag = On
○ session.gc_maxlifetime = 10800
○ date.timezone = Europe/Madrid
○ upload_max_filesize = 10M),
seteados correctamente
----
$exit


														### VM AWS-WWW
														-----------
----Nos conectamos
$ssh -l centos -i /Users/Santt/Desktop/www_key.pem  52.210.139.138
----Verificamos que ese nginx
$sudo service nginx start
----Verificamos fpm
$php-fpm -v
----Verificamos PHP con los módulos php-mcrypt, php-gd, php-mbstring
$cat /etc/php.ini

(se podran ver los valores como ○ short_open_tag = On
○ session.gc_maxlifetime = 8000
○ date.timezone = Europe/Madrid
○ upload_max_filesize = 2M),
seteados correctamente




											###COMUNES(VM DEVEL Y AWS LO TIENEN )
											------------------------------------

$php --version
$cat /etc/php.ini
(se podran ver los valores como short_open_tag,session.gc_maxlifetime, date.timezone, upload_max_filesize)
seteados correctamente







